using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ViveController : MonoBehaviour
{

    public static ViveController instance;

    public SteamVR_Input_Sources leftHand = SteamVR_Input_Sources.LeftHand;
    public SteamVR_Input_Sources rightHand = SteamVR_Input_Sources.RightHand;
    public SteamVR_Input_Sources any = SteamVR_Input_Sources.Any;

    public SteamVR_Action_Boolean trigger = SteamVR_Actions.default_InteractUI;
    public SteamVR_Action_Vibration haptic = SteamVR_Actions.default_Haptic;

    public HandMotionState leftHandState = HandMotionState.Idle;
    public HandMotionState rightHandState = HandMotionState.Idle;

    void Awake()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(trigger.GetState(leftHand))
        {
            leftHandState = HandMotionState.trigger;
        }
        else
        {
            leftHandState = HandMotionState.Idle;
        }
        
        if(trigger.GetState(rightHand))
        {
            rightHandState = HandMotionState.trigger;
        }
        else
        {
            rightHandState = HandMotionState.Idle;
        }
    }
}
