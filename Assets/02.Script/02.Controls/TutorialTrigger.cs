using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    private GameObject tutorPannel;

    public bool lastBlock;

    private PlayerControl playerControl;

    
    void Start()
    {
        playerControl = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("PLAYER_BODY"))
        {
            StartCoroutine(SlowDown());
            //tutorPannel.SetActive(false);
            Invoke("DestroyPanel",5.0f);
        }
    }

    IEnumerator SlowDown()
    {

        while(playerControl.moveSpeed > 0.5f)
        {
            playerControl.SetSpeed(playerControl.moveSpeed / 1.3f);

            yield return new WaitForSeconds(0.1f);
        }

        playerControl.SetSpeed(0);
    }

    void DestroyPanel()
    {
        Destroy(this.gameObject);
        if(!lastBlock)
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>().SetSpeed(8);
        }
        else
        {
            GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>().SetSpeed(1.5f);
        }
    }
}
