using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MomentInteractionControl : MonoBehaviour
{
    public MomentInteractionType type = MomentInteractionType.trigger;

    private GameObject effectPrefab;

    private GameObject effectObj;



    void Start()
    {
        this.effectPrefab = GameObject.FindGameObjectWithTag("GAMEMANAGER")?.GetComponent<DataManager>()?.momentEffect;
    }

    public void ShowEffect()
    {
        if(this.effectPrefab == null) return;

        this.effectObj = Instantiate(this.effectPrefab, transform.position, transform.rotation);


        Invoke("Destory", 5.0f);
    }

    void Destroy()
    {
        Destroy(this.effectObj);
    }
}
