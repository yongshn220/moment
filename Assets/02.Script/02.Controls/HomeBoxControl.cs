
using UnityEngine;

public class HomeBoxControl : MonoBehaviour
{
    private GameObject playerObj;
    //public HomeBoxType type;

    //KHG
    public GameObject good;
    public GameObject bad;

    // Start is called before the first frame update
    void Start()
    {
        playerObj = GameObject.FindGameObjectWithTag("Player");

        PlayerControl pc = playerObj.GetComponent<PlayerControl>();


        //KHG
        if(pc.SP<50)
        {
            good.SetActive(false);
        }
        else
        {
            bad.SetActive(false);
        }
        pc.resetSP();
    }
}
