using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class MomentZoneControl : MonoBehaviour
{

    public GameObject[] moments;

    public event EventHandler momentInteract;

    // Event Call : MomentControl 'OnMomentInteract()' method.
    public void OnMomentInteract()
    {
        momentInteract.Invoke(this, EventArgs.Empty);
    }
}
