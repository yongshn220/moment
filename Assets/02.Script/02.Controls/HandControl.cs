using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;


public class HandControl : MonoBehaviour
{   
    //Player Object
    private GameObject playerObj;

    public HandType handType;

    
    void Start()
    {
        playerObj = GameObject.FindGameObjectWithTag("Player");
    }

#region CALL_BACK

    // Hand enter Moment Interaction point.
    void OnTriggerEnter(Collider coll)
    {
        // Object interaction
        if(coll.CompareTag("OBJECT"))
        {
            var objCtrl = coll.GetComponent<ObjectControl>();
            OnObjectInteract(objCtrl);


            if(objCtrl.type == ObjectType.Wall)
            {
                playerObj.GetComponent<PlayerControl>().OnVignette();
            }

            // Sound
            playerObj.GetComponent<AudioSource>().PlayOneShot(coll.GetComponent<AudioSource>().clip);
            Destroy(coll.gameObject);
        }

    }

    // Hand inside Moment Interaction point.
    void OnTriggerStay(Collider coll)
    {
        if(coll.CompareTag("MOMENT_INTERACTION"))
        {
            var momentInterCtrl = coll.gameObject.GetComponent<MomentInteractionControl>();  

            // If Pinch interaction valids,
            if(TryMomentTriggerEvent(momentInterCtrl))
            {
                // disable collider for avoiding interact multiple times.
                coll.gameObject.GetComponent<Collider>().enabled = false;
            }
        }
    }
#endregion


#region OBJECT_INTERACTION

    void OnObjectInteract(ObjectControl objCtrl)
    {
        Object obj = objCtrl.obj;

        PlayerGetObjectPoint(obj.MO, obj.HP, obj.HA);
    }
#endregion


#region MOMENT_INTERACTION

    // Moment Pinch Interation
    bool TryMomentTriggerEvent(MomentInteractionControl miCtrl)
    {   
        ViveController vc = ViveController.instance;

        if(handType == HandType.left)
        {
            if(vc.leftHandState == HandMotionState.trigger)
            {
                InteractionValid(miCtrl, vc.leftHand);
                return true;
            }
        }
        else if(handType == HandType.right)
        {
            if(vc.rightHandState == HandMotionState.trigger)
            {
                InteractionValid(miCtrl, vc.rightHand);
                return true;
            }
        }
        return false;
    }

    void InteractionValid(MomentInteractionControl miCtrl, SteamVR_Input_Sources hand)
    {
        //haptic
        ViveController.instance.haptic.Execute(0f, 0.5f, 200f, 0.5f, hand);

        //Moment event
        MomentControl momentCtrl = miCtrl.gameObject.transform.parent.GetComponent<MomentControl>();
        OnMomentInteract(momentCtrl);

        //Particle Effect
        miCtrl.ShowEffect();

        //Sound
        AudioClip clip = GameManager.instance.gameObject.GetComponent<DataManager>().momentSfx;
        playerObj.GetComponent<PlayerControl>().PlayAudio(clip);
    }

    // Get Moment Control Script and send its properties to Player Control.
    void OnMomentInteract(MomentControl momentCtrl)
    {   
        // Call Moment Zone Interaction Event Method.
        MomentZoneControl mzc =  momentCtrl.transform.parent.GetComponent<MomentZoneControl>();

        // Get Moment class
        Moment m = momentCtrl.moment;

        // Add Point
        PlayerGetMomentPoint(m.MO, m.HP, m.HA, m.cSP, m.tSP, m.ySP, m.aSP);

        // reduce player speed (slow motion)
        playerObj.GetComponent<PlayerControl>().MotionSlow(mzc);

        // add checked moment in gameManager (for ending scene)
        GameManager.instance.AddInteractedMoment(momentCtrl.type);
    }
#endregion  


#region PLAYER_SEND
    void PlayerGetMomentPoint(int MO, int HP, int HA, int cSP, int tSP, int ySP, int aSP)
    {
        Debug.Log("Moment!!!");
        playerObj.GetComponent<PlayerControl>().GetPoint(MO, HP, HA, cSP, tSP, ySP, aSP);
    }

    void PlayerGetObjectPoint(int MO, int HP, int HA)
    {
        playerObj.GetComponent<PlayerControl>().GetPoint(MO, HP, HA, 0, 0, 0, 0);
    }
}
#endregion
