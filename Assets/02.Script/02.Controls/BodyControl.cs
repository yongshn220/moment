using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyControl : MonoBehaviour
{

    private GameObject playerObj;  

    void Start()
    {
        playerObj = GameObject.FindGameObjectWithTag("Player");
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("OBJECT"))
        {
            var objCtrl = coll.gameObject.GetComponent<ObjectControl>();
            

            OnObjectInteract(objCtrl);

            if(coll.GetComponent<ObjectControl>().type == ObjectType.Wall)
            {
                Debug.Log("vig compare");
                playerObj.GetComponent<PlayerControl>().OnVignette();
                GameObject.FindGameObjectWithTag("Player").GetComponent<AudioSource>().PlayOneShot(coll.GetComponent<AudioSource>().clip);
            }

            Destroy(coll.gameObject);
        }
    }

    void OnObjectInteract(ObjectControl objCtrl)
    {
        Object obj = objCtrl.obj;

        PlayerGetObjectPoint(obj.MO, obj.HP, obj.HA);

        Debug.Log("Object Interacted. type : " + obj.objectType);
    }

    void PlayerGetObjectPoint(int MO, int HP, int HA)
    {
        playerObj.GetComponent<PlayerControl>().GetPoint(MO, HP, HA, 0, 0, 0, 0);
    }
}
