using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MemoryControl : MonoBehaviour
{
    private float transparancy;

    private Material material;

    //KHG
    void Start()
    {
        material = this.transform.parent.GetComponent<Renderer>().material;
        Debug.Log("this:"+this.gameObject);
        Debug.Log("parent:"+this.transform.parent.gameObject);
        Debug.Log(material);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider coll)
    {
        //KHG
        if(coll.CompareTag("PLAYER_BODY"))
        {
            
            StartCoroutine(FadeOut());
        }
    }

    
    IEnumerator FadeOut()
    {
        while(material.color.a >= 0)
        {
            this.material.color = new Color(1,1,1, this.material.color.a - 0.08f);
            yield return new WaitForSeconds(0.10f);
        }
    }
}
