using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;
using Valve.VR.Extras;


public class PlayerControl : MonoBehaviour
{   
    //Dev
    public TMP_Text mo;
    public TMP_Text hp;
    public TMP_Text ha;

    //Transform
    private Transform tr;
    
    //Speed of player
    public float moveSpeed = 13.0f;
    public float SPEED = 13.0f;


    //Properties
    public int MO = 0;
    public int HP = 0;
    public int HA = 0;
    public int SP = 0;
    public SceneState state;

    public Transform bodyCollider;

    //Manager
    private GameManager gameManager;

    private UIManager uIManager;

    //Event
    public PropertiesEvent getPointOnUIEvent;
    public PropertiesEvent lackOfPointEvent;

    //Vignette
    public Image vignette;

    //KHG
    //HomeBox
    public TMP_Text job;
    public GameObject jobImage;

    private AudioSource source;

    public GameObject VRUI;

    public GameObject HandLeft;
    public GameObject HandRight;

    void Awake()
    {
        this.tr = this.GetComponent<Transform>();
    }
    void Start()
    {
        
        this.bodyCollider.GetComponent<SceneControl>().newSceneLoad += OnNewSceneLoad;

        gameManager = GameManager.instance;

        uIManager = gameManager.gameObject.GetComponent<UIManager>();


        vignette.CrossFadeAlpha(0,0,true);// Vignating image Alpha value


        vignette.color = new Color(1,1,1,245/255f);

        //Player Start Setting
        StartCoroutine(StartSetting());

        source = GetComponent<AudioSource>();

        this.state = gameManager.sceneState;
    }

    IEnumerator StartSetting()
    {
        this.MO = 50;
        this.HP = 50;
        this.HA = 50;
        this.SP = 0;
        this.moveSpeed = 13.0f;

        yield return new WaitForSeconds(0.1f);
        uIManager.OnColorUpdate();
    }

    void Update()
    {
        MoveForward();
    }

    void DevTmp()
    {
        mo.text = this.MO.ToString();
        hp.text = this.HP.ToString();
        ha.text = this.HA.ToString();
    }

    void MoveForward()
    {
        if(GameManager.instance.sceneState != SceneState.Lobby)
        {
            this.tr.position += Vector3.forward * moveSpeed * Time.deltaTime;
        }
    }

    //Event
    void OnNewSceneLoad(object sender, EventArgs args)
    {
        //KHG
        if(this.state != SceneState.Ending)
        {
            this.state = gameManager.sceneState;
            ResetPosition();
        }
    }

    
    public void resetSP()
    {
       //KHG
       OnHomeBox(this.state, this.SP);
       this.SP = 0;
    }

    void ResetPosition()
    {
        this.tr.position = new Vector3(this.tr.position.x, this.tr.position.y, 0);
    }

    //Add points into its properties if player interacts with Moments or Objects.
    public void GetPoint(int _MO, int _HP, int _HA, int _cSP, int _tSP, int _ySP, int _aSP)
    {
        this.MO += _MO;
        this.HP += _HP;
        this.HA += _HA;

        PointAdjust();

        switch(this.state)
        {
            case SceneState.Child : this.SP += _cSP;
            break;
            case SceneState.Teenager : this.SP += _tSP;
            break;
            case SceneState.Youth : this.SP += _ySP;
            break;
            case SceneState.Adult : this.SP += _aSP;
            break;
        }

        //UI - property color
        uIManager.OnColorUpdate();

        //Event call (GetPointControl.OnGetPoint())
        getPointOnUIEvent.Invoke(_MO, _HP, _HA);
        lackOfPointEvent.Invoke(this.MO, this.HP, this.HA);

    }

    // set properties 0 <= value <= 100
    void PointAdjust()
    {
        if(this.MO > 100)
        {
            this.MO = 100;
        }

        if(this.HP > 100)
        {
            this.HP = 100;
        }

        if(this.HA > 100)
        {
            this.HA = 100;
        }

        if(this.MO < 0)
        {
            this.MO = 0;
        }

        if(this.HP < 0)
        {
            this.HP = 0;
        }
        
        if(this.HA < 0)
        {
            this.HA = 0;
        }
    }

    public void OnEnterHome()
    {
        
        Debug.Log("OnEnter");
        this.moveSpeed = 0.0f;

        if(this.gameManager.sceneState == SceneState.Child)
        {
            StartCoroutine(StartSetting());
            Invoke("NormalSpeed", 3.0f);
        }

        else if(this.gameManager.sceneState == SceneState.Lobby)
        {
            Debug.Log("OnEnter else if");
            OnLobbyEnter();
            return;
        }

        else
        {
            //Event
            lackOfPointEvent.Invoke(this.MO, this.HP, this.HA);
            Invoke("NormalSpeed", 5.0f);
        }

        this.VRUI.SetActive(true);
    }


#region PLAYER_STAT

    //slow player speed if interacts moment.
    public IEnumerator OnMotionSlow(MomentZoneControl mzc)
    {   
        float slowSpeed = 0.5f;

        while(this.moveSpeed > slowSpeed)
        {
            this.moveSpeed = this.moveSpeed / 1.15f;

            if(this.moveSpeed < slowSpeed)
            {
                this.moveSpeed = slowSpeed;
            }

            yield return new WaitForSeconds(0.01f);
        }

        yield return new WaitForSeconds(2.0f);

        NormalSpeed();
        mzc.OnMomentInteract();
    }

    public void MotionSlow(MomentZoneControl mzc)
    {
        StartCoroutine(OnMotionSlow(mzc));
    }

    // reset player speed 
    public void NormalSpeed()
    {
        this.moveSpeed = this.SPEED;
    }

    public void SetSpeed(float speed)
    {
        this.moveSpeed = speed;

    }

    // Start is called before the first frame update    
    public void OnVignette()
    {
        vignette.CrossFadeAlpha(1,0,true);
        vignette.CrossFadeAlpha(0,2.5f,true);

    }

    //KHG
    void OnHomeBox(SceneState state, int sp)
    {
        jobImage.SetActive(true);
        if(sp<50)
        {
            if(state == SceneState.Teenager) {job.text="<color=#FF004C>'비행청소년'</color><color=#000000>이 되었다.</color>";}
            else if(state == SceneState.Youth) {job.text="<color=#FF004C>'백수'</color><color=#000000>가 되었다.</color>";}
            else if(state == SceneState.Adult) {job.text="<color=#FF004C>'백수'</color><color=#000000>가 되었다.</color>";}
            else if(state == SceneState.Senior) {job.text="<color=#FF004C>'노숙자'</color><color=#000000>가 되었다.</color>";}
            else {jobImage.SetActive(false);}
        }
        else if(sp>=50)
        {
            if(state == SceneState.Teenager) {job.text="<color=#006FFF>'모범생'</color><color=#000000>이 되었다.</color>";}
            else if(state == SceneState.Youth) {job.text="<color=#006FFF>'대학생'</color><color=#000000>이 되었다.</color>";}
            else if(state == SceneState.Adult) {job.text="<color=#006FFF>'CEO'</color><color=#000000>가 되었다.</color>";}
            else if(state == SceneState.Senior) {job.text="<color=#006FFF>'건강한노인'</color><color=#000000>이 되었다.</color>";}
            else {jobImage.SetActive(false);}
        }
        Invoke("OffHomeBox",3f);
    }
    //KHG
    void OffHomeBox()
    {
        jobImage.SetActive(false);
    }

    public void PlayAudio(AudioClip clip)
    {
        source.PlayOneShot(clip);
    }

    public void OnLobbyEnter()
    {
        this.moveSpeed = 0;
        this.VRUI.SetActive(false);
    }

    public void LaserOff()
    {
            Debug.Log("laser off");
        this.HandLeft.GetComponent<SteamVR_LaserPointer>().enabled = false;
        this.HandLeft.transform.Find("New Game Object").gameObject.SetActive(false);
        this.HandRight.GetComponent<SteamVR_LaserPointer>().enabled = false;
        this.HandRight.transform.Find("New Game Object").gameObject.SetActive(false);
    }

    public void LaserOn()
    {
           Debug.Log("laser on");
        this.HandLeft.GetComponent<SteamVR_LaserPointer>().enabled = true;
        this.HandLeft.transform.Find("New Game Object").gameObject.SetActive(true);
        this.HandRight.GetComponent<SteamVR_LaserPointer>().enabled = true;
        this.HandRight.transform.Find("New Game Object").gameObject.SetActive(true);
    }

#endregion
}