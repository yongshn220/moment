using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MomentControl : MonoBehaviour
{
    public MomentType type;
    public Moment moment;

    public int MO;
    public int HP;
    public int HA;
    public int cSP;
    public int tSP;
    public int ySP;
    public int aSP;


    public GameObject interactionPoint;
    public GameObject disableBox;

    private GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        // Player
        player = GameObject.FindGameObjectWithTag("Player");

        // Info
        moment = DataManager.instance.momentData.GetMoment(type);

        // Properties for Inspector view
        MO = moment.MO;
        HP = moment.HP;
        HA = moment.HA;
        cSP = moment.cSP;
        tSP = moment.tSP;
        ySP = moment.tSP;
        aSP = moment.aSP;


        // Turn off interaction if other or its moment interacted.
        transform.parent.GetComponent<MomentZoneControl>().momentInteract += OnMomentInteractOff;
        
        // Turn off interaction if player is lack of points, Turn on if enough.
        player.GetComponent<PlayerControl>().lackOfPointEvent.AddListener(LackOfPointEvent);
    }

    //Turn off Interaction.
    void OnMomentInteractOff(object sender, EventArgs args)
    {
        disableBox.SetActive(true);
        interactionPoint.SetActive(false);
    }

    // Turn on Interaction.
    void OnMomentInteractOn()
    {
        disableBox.SetActive(false);
        interactionPoint.SetActive(true);
    }

    // turn on/off moment depends on the player points
    void LackOfPointEvent(int player_MO, int player_HP, int player_HA)
    {
        if(IsPointValid(player_MO, player_HP, player_HA))
        {
            OnMomentInteractOff(this, EventArgs.Empty);
        }
        else
        {
            OnMomentInteractOn();
        }
    }

    // check if player has enough points or not.
    bool IsPointValid(int pMO, int pHP, int pHA)
    {
        if((this.MO + pMO) < 0 || (this.HP + pHP) < 0 || (this.HA +pHA) < 0)
        {   
            return true;
        }
        return false;
    }
}
