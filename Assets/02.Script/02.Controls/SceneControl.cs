using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class SceneControl : MonoBehaviour
{

    public event EventHandler newSceneLoad;

    public bool sceneLoop = false;

    public GameObject playerObj;

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("SCENE_END_POINT"))
        {
            SteamVR_Fade.Start(Color.black, 2.0f);
        }

        if(coll.CompareTag("SCENE_START_POINT"))
        {
            Invoke("FadeClear", 0.5f);
            playerObj.GetComponent<PlayerControl>().OnEnterHome();
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if(coll.CompareTag("SCENE_END_POINT"))
        {
            GameManager.instance.LoadScene(coll.GetComponent<EndPointControl>().nextScene);
            newSceneLoad.Invoke(this, EventArgs.Empty);
        }
    }

    void FadeClear()
    {
        SteamVR_Fade.Start(Color.clear, 1.0f);
    }
}
