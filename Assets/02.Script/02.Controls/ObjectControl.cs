using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectControl : MonoBehaviour
{
    public ObjectType type;
    public Object obj;

    public int MO;
    public int HP;
    public int HA;

    // Start is called before the first frame update
    void Start()
    {
        obj = DataManager.instance.objectData.GetObject(type);

        MO = obj.MO;
        HP = obj.HP;
        HA = obj.HA;
    }
    
}
