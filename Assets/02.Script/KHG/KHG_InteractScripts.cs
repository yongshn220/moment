using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KHG_InteractScripts : MonoBehaviour
{
    // Start is called before the first frame update

    private int rnd;
    public bool isRotate = false;
    public bool isMove = true;
    void Start()
    {
        rnd = Random.Range(50,100);
        this.transform.position -= Vector3.up*1.5f;

        
        StartCoroutine("UpDown");
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    IEnumerator UpDown()
    {
        while(true)
        {
            for(int i=0;i<rnd;i++)
            {
                if(isMove)this.transform.position += Vector3.up*0.03f;
                if(isRotate)this.transform.Rotate(Vector3.up*3);
                yield return new WaitForSeconds(0.01f);
            }
            for(int i=0;i<rnd;i++)
            {
                if(isMove)this.transform.position -= Vector3.up*0.03f;
                if(isRotate)this.transform.Rotate(Vector3.up*3);
                yield return new WaitForSeconds(0.01f);
            }
        }
    }
}
