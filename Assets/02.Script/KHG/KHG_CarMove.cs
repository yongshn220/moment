using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KHG_CarMove : MonoBehaviour
{
    public float i=1;
    Vector3 vec;
    public List<GameObject> wheelList = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        vec = new Vector3(0,0,i);
        if(gameObject.name=="Sports_1")
        {
            StartCoroutine("Right");
            vec = Vector3.right*i;

        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
        this.transform.position += vec*Time.deltaTime;

        foreach(var wheel in wheelList)
        {
            wheel.transform.Rotate(Vector3.right*2);
        }
    }

    IEnumerator Right()
    {
        for(int i=0;i<1;i++)
        {
            yield return new WaitForSeconds(28.0f);
            for(int j=0;j<90;j++)
            {
                this.transform.Rotate(Vector3.up);
                yield return new WaitForSeconds(0.02f);
            }
        }
        vec = Vector3.back*i;
    }
}
