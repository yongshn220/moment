using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KHG_ObjectPosition : MonoBehaviour
{

    public int range1=0;
    public int range2=0;
    public float scale=1.5f;

    public bool coin=false;

    private float rot;

    private bool isOk = false;
    // Start is called before the first frame update
    void Start()
    {
        this.transform.localScale *= scale;
        rot = Random.Range(3f,6f);
        SetPosition();
        Invoke("CheckWall2",0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        
        if(coin){this.transform.Rotate(Vector3.up*rot);}

        if(isOk==true) return;
        CheckWall();
        if(!isOk)SetPosition();
        

    }

    void CheckWall()
    {
        Collider[] coll = Physics.OverlapBox(this.transform.position,Vector3.one*0.3f*scale,Quaternion.identity);
        if(coll.Length<=1){isOk=true;}
        else{isOk=false;}
        //Destroy(this.gameObject);
    }
    void CheckWall2()
    {
        Collider[] coll = Physics.OverlapBox(this.transform.position,Vector3.one*0.3f*scale,Quaternion.identity);
        if(coll.Length>1)
        Destroy(this.gameObject);
    }

    void SetPosition()
    {
        this.transform.position = Vector3.zero;
        this.transform.position += Vector3.forward * Random.Range(range1/3,range2/3+1)*3;
        this.transform.position += Vector3.up * (0+Random.Range(1,3)*1f);
        this.transform.position += Vector3.right *(-1+ Random.Range(0,3)*1f);

    }

}
