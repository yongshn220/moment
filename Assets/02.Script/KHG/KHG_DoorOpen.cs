
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class KHG_DoorOpen : MonoBehaviour
{

    public GameObject leftDoor;
    public GameObject rightDoor;

    public bool isFirst=false;

    //! 플레이어 찾을것
    private GameObject Player;


    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");

    }


    private bool door = false;
    // Update is called once per frame
    void Update()
    {
        if(door) return;
        if(isFirst)
        {
            Invoke("OpenDoorStart",3.0f);  
            door = true;   
        }
        if(!isFirst&&Vector3.Distance(Player.transform.position,this.transform.position)<35)
        {
            StartCoroutine("OpenDoor");
            door = true;
        }
    }

    void OpenDoorStart()
    {
        StartCoroutine("OpenDoor");
    }

    IEnumerator OpenDoor()
    {
        
        for(int i=0;i<70;i++)
        {
            leftDoor.transform.Rotate(Vector3.down*2);
            rightDoor.transform.Rotate(Vector3.up*2);

            yield return new WaitForSeconds(0.01f);
        }
    }
}
