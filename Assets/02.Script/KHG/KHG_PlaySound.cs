using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KHG_PlaySound : MonoBehaviour
{

    private AudioSource aud;
    public AudioClip sfx;
    public float dis;

    private GameObject player;
    public bool isPlay=false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        aud = this.gameObject.AddComponent<AudioSource>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isPlay)return;

        if(Vector3.Distance(player.transform.position,this.transform.position)<dis)
        {
            isPlay = true;
            aud.PlayOneShot(sfx);
        }

    }
}
