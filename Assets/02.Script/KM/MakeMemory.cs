using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeMemory : MonoBehaviour
{


    private new MeshRenderer renderer;

    public GameObject memoryCtrl;



    void Start()
    {

        MakeMemoryQuad();
        MemoryMatChangeMgr();

    }





    void Update()
    {
        
    }



    void MakeMemoryQuad()
    {
        
        GameObject obj = Instantiate((GameObject)Resources.Load("Memory") , new Vector3(0,1.25f,30) , Quaternion.identity);

        Instantiate((GameObject)Resources.Load("Memory") , new Vector3(0,1.25f,60) , Quaternion.identity);
        Instantiate((GameObject)Resources.Load("Memory") , new Vector3(0,1.25f,90) , Quaternion.identity);
        Instantiate((GameObject)Resources.Load("Memory") , new Vector3(0,1.25f,120) , Quaternion.identity);
        Instantiate((GameObject)Resources.Load("Memory") , new Vector3(0,1.25f,150) , Quaternion.identity);
        Color c = obj.GetComponent<MeshRenderer>().material.color;
        obj.GetComponent<MeshRenderer>().material.color = new Color(c.r , c.g, c.b, 0.8f);


    
    }



    void MemoryMatChangeMgr()
    {
        memoryCtrl = GameObject.Find("Memory(Clone)");
        renderer = memoryCtrl.GetComponent<MeshRenderer>();

        // renderer.GetComponent<MeshRenderer>().material.color = new Color(1,1,1,1);




    }





}
