using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Valve.VR;
using Valve.VR.Extras;

public class LobbyUIEventCast : MonoBehaviour
{
    private SteamVR_LaserPointer laserPointer;


    void Awake()
    {
        laserPointer = GetComponent<SteamVR_LaserPointer>();
    }

    void OnEnable()
    {
        laserPointer.PointerIn += OnPointerEnter;
        laserPointer.PointerOut += OnPointerExit;
        laserPointer.PointerClick += OnPointerClick;
    }

    void OnDisable()
    {
        laserPointer.PointerIn -= OnPointerEnter;
        laserPointer.PointerOut -= OnPointerExit;
        laserPointer.PointerClick -= OnPointerClick;
    }

    void OnPointerEnter(object sender, PointerEventArgs e)
    {
        var eventHandler = e.target.GetComponent<IPointerEnterHandler>();

        if(eventHandler == null) return;

        Debug.Log("Enter");
        eventHandler.OnPointerEnter(new PointerEventData(EventSystem.current));
    }

    void OnPointerExit(object sender, PointerEventArgs e)
    {
        var eventHandler = e.target.GetComponent<IPointerExitHandler>();

        if(eventHandler == null) return;

        Debug.Log("Exit");
        eventHandler.OnPointerExit(new PointerEventData(EventSystem.current));
    }

    void OnPointerClick(object sender, PointerEventArgs e)
    {
        var eventHandler = e.target.GetComponent<IPointerClickHandler>();

        if(eventHandler == null) return;

        Debug.Log("Click");
        eventHandler.OnPointerClick(new PointerEventData(EventSystem.current));
    }
}
