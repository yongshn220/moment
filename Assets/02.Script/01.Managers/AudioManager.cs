using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioClip lobby;
    public AudioClip infant;
    public AudioClip child;
    public AudioClip teenager;
    public AudioClip youth;
    public AudioClip adult;
    public AudioClip senior;
    public AudioClip ending;

    private Dictionary<SceneState, AudioClip> clipDict = new Dictionary<SceneState, AudioClip>();
    
    private AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        source = this.transform.GetComponent<AudioSource>();
        clipDict.Add(SceneState.Lobby, lobby);
        clipDict.Add(SceneState.Infant, infant);
        clipDict.Add(SceneState.Child, child);
        clipDict.Add(SceneState.Teenager, teenager);
        clipDict.Add(SceneState.Youth, youth);
        clipDict.Add(SceneState.Adult, adult);
        clipDict.Add(SceneState.Senior, senior);
        clipDict.Add(SceneState.Ending, ending);
    }

    void Play(SceneState state)
    {
        Stop();
        AudioClip newClip;
        clipDict.TryGetValue(state, out newClip);
        source.clip = newClip;
        source.Play();
    }

    void Stop()
    {
        source.Stop();
    }
}
