using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using UnityEngine.SceneManagement;



public class _TestingManager : MonoBehaviour
{
    public SteamVR_Action_Boolean trigger = SteamVR_Actions.default_InteractUI;
    public SteamVR_Input_Sources any = SteamVR_Input_Sources.Any;

    public enum _NextSceneType
    {
        Child,
        Teenager,
        Youth,
        Adult,
        Senior,
        Ending,
    }

    public _NextSceneType nextScene;
    // Update is called once per frame
    void Update()
    {
        if(trigger.GetState(any) || Input.GetKeyDown(KeyCode.Space))
        {
            SceneManager.LoadScene(this.nextScene.ToString());
        }
    }
}
