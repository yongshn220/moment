using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField]
    public SceneState sceneState = SceneState.Lobby;

    public List<MomentType> InteractedMomentList = new List<MomentType>();

    public GameObject player;

    void Awake()
    {   
        if(instance != null)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance =  this;
            DontDestroyOnLoad(this.gameObject);
            
        }
    }

    public void LoadScene(SceneState state)
    {
        SceneManager.LoadScene(state.ToString(), LoadSceneMode.Single);
        this.sceneState = state;

        if(state == SceneState.Lobby)
        {
            this.InteractedMomentList = new List<MomentType>();
            this.player.GetComponent<PlayerControl>().LaserOn();
        }
        else if(state == SceneState.Infant)
        {
            Debug.Log("infant scene");
            this.player.GetComponent<PlayerControl>().LaserOff();
            this.player.GetComponent<PlayerControl>().VRUI.SetActive(true);
        }
    }

    bool NextSceneValid()
    {
        return true;
    }
    
    public void AddInteractedMoment(MomentType mt)
    {  
        if(!InteractedMomentList.Contains(mt))
        {
            InteractedMomentList.Add(mt);
        }
    }

    public List<MomentType> GetInteractedMoment()
    {
        return this.InteractedMomentList;
    }
}
