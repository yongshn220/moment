using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class DataManager : MonoBehaviour
{
    
    public static DataManager instance;

    public ObjectData objectData = new ObjectData();

    public MomentData momentData = new MomentData(); 
    
    public GameObject momentEffect;

    public AudioClip momentSfx;

    //moment picture objects (for Ending Scene)
    public GameObject[] memoryPrefabs;



    void Awake()
    {
        instance = this;
    }
}

#region MOMENT

public class MomentData
{
    public List<Moment> momentList = new List<Moment>();

    public MomentData() 
    {
        momentList.Add(new Moment(MomentType.CatchBall,    0, -1, 1, 25, 25, 25, 25));
        momentList.Add(new Moment(MomentType.Park,         0, -1, 1, 10, 10, 10, 10));
        momentList.Add(new Moment(MomentType.Kindergarten, 0, -1, -1, 20, 20, 20, 20));
        momentList.Add(new Moment(MomentType.Toy,          0, 0, 1, 15, 15, 15, 15));
        momentList.Add(new Moment(MomentType.PartTime,     30, -15, -5, 10, 10, 10, 10));
        momentList.Add(new Moment(MomentType.Study,        -5, -5, -5, 20, 20, 20, 20));
        momentList.Add(new Moment(MomentType.Date,         -15, -5, 15, 10, 10, 10, 10));
        momentList.Add(new Moment(MomentType.Hobby,        -5, -5, 10, 15, 15, 15, 15));
        momentList.Add(new Moment(MomentType.WorkOut,      0, 15, 0, 15, 15, 15, 15));
        momentList.Add(new Moment(MomentType.Game,         -10, 0, 10, 10, 10, 10, 10));
        momentList.Add(new Moment(MomentType.Friend,       0, 0, 20, 10, 10, 10, 10));
        momentList.Add(new Moment(MomentType.Misconduct,   0, 0, 0, 5, 5, 5, 5));
        momentList.Add(new Moment(MomentType.Travel,       -30, -5, 30, 10, 10, 10, 10));
        momentList.Add(new Moment(MomentType.Marriage,     -50, -10, 40, 20, 20, 20, 20));
        momentList.Add(new Moment(MomentType.Work,         30, -15, -5, 15, 15, 15, 15));
        momentList.Add(new Moment(MomentType.Stock,        10, 0, 0, 10, 10, 10, 10));
        momentList.Add(new Moment(MomentType.Gambling,     -40, 0, 0, 10, 10, 10, 10));
        momentList.Add(new Moment(MomentType.Car,          -50, 0, 30, 20, 20, 20, 20));
        momentList.Add(new Moment(MomentType.Shopping,     -10, -5, 15, 10, 10, 10, 10));
        momentList.Add(new Moment(MomentType.Pharmacy,     -20, 20, 10, 0, 0, 0, 0));
        momentList.Add(new Moment(MomentType.NursingHome,  0, 0, 20, 0, 0, 0, 0));
    }

    public Moment GetMoment(MomentType mt)
    {
        foreach (var moment in momentList)
        {
            if(moment.momentType == mt)
            {
                return moment;
            }
        }
        return null;
    }
}

public class Moment
{
    public MomentType momentType;
    public int MO;
    public int HP;
    public int HA;
    public int cSP;
    public int tSP;
    public int ySP;
    public int aSP;

    public Moment(MomentType mt, int MO, int HP, int HA, int cSP, int tSP, int ySP, int aSP)
    {
        this.momentType = mt;
        this.MO = MO;
        this.HP = HP;
        this.HA = HA;
        this.cSP = cSP;
        this.tSP = tSP;
        this.ySP = ySP;
        this.aSP = aSP;
    }
}
#endregion

#region OBJECT

public class ObjectData
{
    public List<Object> objectList = new List<Object>();

    public ObjectData()
    {
        objectList.Add(new Object(ObjectType.MO, 1, 0, 0));
        objectList.Add(new Object(ObjectType.HP, 0, 1, 0));
        objectList.Add(new Object(ObjectType.HA, 0, 0, 1));
        objectList.Add(new Object(ObjectType.Wall, -10, -10, -10));
    }

    public Object GetObject(ObjectType ot)
    {
        foreach (var obj in objectList)
        {
            if(obj.objectType == ot)
            {
                return obj;
            }
        }
        return null;
    }
}

public class Object
{
    public ObjectType objectType;
    public int MO;
    public int HP;
    public int HA;

    public Object(ObjectType ot, int MO, int HP, int HA)
    {
        this.objectType = ot;
        this.MO = MO;
        this.HP = HP;
        this.HA = HA;
    }
}

#endregion 

#region ENUM

//KHG
public enum PlayerState
{
    Infant,
    Child,
    Teenager,
    Youth,
    Adult,
    Senior,
    Ending,
}

public enum ObjectType
{
    MO,
    HP,
    HA,
    Wall,
}
public enum MomentType
{
    CatchBall,
    Park,
    Kindergarten,
    Toy,
    Lie,
    PartTime,
    Study,
    Date,
    Hobby,
    WorkOut,
    Game,
    Friend,
    Misconduct,
    Travel,
    Marriage,
    Work,
    Stock,
    Gambling,
    Car,
    Shopping,
    Pharmacy,
    NursingHome,
}

public enum MomentInteractionType
{
    Touch,
    trigger,
}

public enum HandMotionState
{
    Idle,
    trigger,
}

public enum SceneState
{
    Lobby,
    Infant,
    Child,
    Teenager,
    Youth,
    Adult,
    Senior,
    Ending,
}

public enum HandType
{
    left,
    right,
}

public enum HomeBoxType
{
    Good,
    Bad,
}
#endregion


#region EVENTS
[System.Serializable]
public class PropertiesEvent : UnityEvent<int, int, int>
{
}


#endregion
