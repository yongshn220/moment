using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryDataManager : MonoBehaviour
{
    public GameObject CatchBall;
    public GameObject Park;
    public GameObject Kindergarten;
    public GameObject Toy;
    public GameObject PartTime;
    public GameObject Study;
    public GameObject Date;
    public GameObject Hobby;
    public GameObject WorkOut;
    public GameObject Game;
    public GameObject Friend;
    public GameObject Misconduct;
    public GameObject Travel;
    public GameObject Marriage;
    public GameObject Work;
    public GameObject Stock;
    public GameObject Gambling;
    public GameObject Car;
    public GameObject Shopping;
    public GameObject Pharmacy;
    public GameObject NursingHome;

    //KHG
    public Dictionary<MomentType,GameObject> momentPrefabDict = new Dictionary<MomentType, GameObject>();



    void Awake()
    {   
        momentPrefabDict.Add(MomentType.CatchBall, CatchBall);
        momentPrefabDict.Add(MomentType.Park, Park);
        momentPrefabDict.Add(MomentType.Kindergarten, Kindergarten);
        momentPrefabDict.Add(MomentType.Toy, Toy);
        momentPrefabDict.Add(MomentType.PartTime, PartTime);
        momentPrefabDict.Add(MomentType.Study, Study);
        momentPrefabDict.Add(MomentType.Date, Date);
        momentPrefabDict.Add(MomentType.Hobby, Hobby);
        momentPrefabDict.Add(MomentType.WorkOut, WorkOut);
        momentPrefabDict.Add(MomentType.Game, Game);
        momentPrefabDict.Add(MomentType.Friend, Friend);
        momentPrefabDict.Add(MomentType.Misconduct, Misconduct);
        momentPrefabDict.Add(MomentType.Travel, Travel);
        momentPrefabDict.Add(MomentType.Marriage, Marriage);
        momentPrefabDict.Add(MomentType.Work, Work);
        momentPrefabDict.Add(MomentType.Stock, Stock);
        momentPrefabDict.Add(MomentType.Gambling, Gambling);
        momentPrefabDict.Add(MomentType.Car, Car);
        momentPrefabDict.Add(MomentType.Shopping, Shopping);
        momentPrefabDict.Add(MomentType.Pharmacy, Pharmacy);
        momentPrefabDict.Add(MomentType.NursingHome, NursingHome);

        
    }
}
