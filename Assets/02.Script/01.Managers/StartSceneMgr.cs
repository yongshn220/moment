using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartSceneMgr : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("LoadLobbyScene", 6.0f);
    }

    void LoadLobbyScene()
    {
        SceneManager.LoadScene("Lobby");
    }
}
