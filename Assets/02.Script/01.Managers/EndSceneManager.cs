using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSceneManager : MonoBehaviour
{
    //public GameObject[] spawnPoints;

    private Dictionary<MomentType, GameObject> momentPrefabDict;

    private List<MomentType> interactedMoment;

    public GameObject endBox;


    // Start is called before the first frame update
    void Start()
    {
        momentPrefabDict = GetComponent<MemoryDataManager>().momentPrefabDict;

        interactedMoment = GameManager.instance.GetInteractedMoment();

        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>().SPEED = 10f;

        SetMemories();
    }

    void SetMemories()
    {
        int i=0;
        for (i = 0; i < this.interactedMoment.Count; i++)
        {
            GameObject newMemoryPrefab;


            //KeyCode: MomentType.CatchBall

            //Get Memory Prefab of interacted moment from the Dictionary.
            momentPrefabDict.TryGetValue(this.interactedMoment[ this.interactedMoment.Count -  i - 1], out newMemoryPrefab);
            
            //Get current transform of spawnPoint and instantiate the prefab.
            //Transform curPointTr = this.spawnPoints[i].transform;
            //GameObject memory = Instantiate(newMemoryPrefab, curPointTr.position, Quaternion.identity);
            GameObject memory = Instantiate(newMemoryPrefab, Vector3.forward*(80+i*40.0f)+Vector3.up*2.5f, Quaternion.identity);

            //Add component on memory 
            
            memory.transform.Find("_Coll").gameObject.AddComponent<MemoryControl>();
        }
        Debug.Log(i);

        GameObject eb = Instantiate(endBox, Vector3.forward*(80 + i * 40.0f), Quaternion.identity);
        eb.GetComponent<EndPointControl>().nextScene = SceneState.Lobby;
    }
}
