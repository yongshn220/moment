using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    

    public GameObject MO_Obj;
    public GameObject HP_Obj;
    public GameObject HA_Obj;

    public Color bestColor;
    public Color betterColor;
    public Color goodColor;
    public Color badColor;
    public Color worstColor;

    private GameObject playerObj;

    void Awake()
    {
        playerObj = GameObject.FindGameObjectWithTag("Player");
    }
    
    public void OnColorUpdate()
    {
        PlayerControl pc = playerObj.GetComponent<PlayerControl>();

        if(pc == null) return;

        MO_Obj.GetComponent<Image>().color = GetColorByValue(pc.MO);
        HP_Obj.GetComponent<Image>().color = GetColorByValue(pc.HP);
        HA_Obj.GetComponent<Image>().color = GetColorByValue(pc.HA);
    }

    private Color GetColorByValue(int value)
    {
        if(value > 80)
        {
            return this.bestColor;
        }
        else if( value > 60)
        {
            return this.betterColor;
        }
        else if (value > 40)
        {
            return this.goodColor;
        }
        else if (value > 20)
        {
            return this.badColor;
        }
        else
        {
            return this.worstColor;
        }
    }
}
