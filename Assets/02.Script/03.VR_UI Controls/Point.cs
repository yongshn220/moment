using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Point : MonoBehaviour
{
    private GameObject value;
    private GameObject up;
    private GameObject down;

    private int num = 0;

    void Start()
    {
        value = transform.Find("Value").gameObject;
        up = transform.Find("Up").gameObject;
        down = transform.Find("Down").gameObject;

        StartSetting();
    }

    public void ShowNewPoint(int val)
    {   
        if(val == 0)
        {
            return;
        }
        else if(val > 0)
        {
            this.up.GetComponent<Image>().enabled = true;
            this.down.GetComponent<Image>().enabled = false;
        }
        else
        {
            this.up.GetComponent<Image>().enabled = false;
            this.down.GetComponent<Image>().enabled = true;
        }   

        value.GetComponent<TMP_Text>().text = Mathf.Abs(val).ToString();
        this.num++;

        Invoke("StartSetting", 5.0f);
    }

    void StartSetting()
    {
        this.num--;
        if(this.num <= 0)
        {
            this.value.GetComponent<TMP_Text>().text = "";
            this.up.GetComponent<Image>().enabled = false;
            this.down.GetComponent<Image>().enabled = false;
            
            this.num = 0;
        }
    }
}
