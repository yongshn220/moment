using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GetPointControl : MonoBehaviour
{
    private Transform MO_Obj;
    private Transform HP_Obj;
    private Transform HA_Obj;

    private PlayerControl pc;
    // Start is called before the first frame update
    void Start()
    {
        MO_Obj = transform.Find("Panel - MO");
        HP_Obj = transform.Find("Panel - HP");
        HA_Obj = transform.Find("Panel - HA");

        pc = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerControl>();

        pc.getPointOnUIEvent.AddListener(OnGetPointUI);
    }

    void OnGetPointUI(int MO, int HP, int HA)
    {
        MO_Obj.GetComponent<Point>().ShowNewPoint(MO);
        HP_Obj.GetComponent<Point>().ShowNewPoint(HP);
        HA_Obj.GetComponent<Point>().ShowNewPoint(HA);
    }
}
