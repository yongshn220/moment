using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyCarMoveCtrl : MonoBehaviour
{
    public GameObject wayPointGroup;

    public Transform[] points;
    public int nextIdx = 1;

    private new Transform transform;
    // private Transform playerTr;

    public float moveSpeed = 9.0f;
    public float damping = 6.0f;
    // public float traceDist = 5.0f;

    private Vector3 movePos;

    // Start is called before the first frame update
    void Start()
    {
        transform = GetComponent<Transform>();
        points = wayPointGroup.GetComponentsInChildren<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        movePos = points[nextIdx].position;

        Vector3 dir = movePos - transform.position;
        Quaternion rot = Quaternion.LookRotation(-dir);

        transform.rotation = Quaternion.Slerp(transform.rotation, rot, Time.deltaTime * damping);

        transform.Translate(-Vector3.forward * Time.deltaTime * moveSpeed);
    }

    void OnTriggerEnter(Collider coll)
    {
        if(coll.CompareTag("WAY_POINT"))
        {
            nextIdx = (++nextIdx >= points.Length) ? 1 : nextIdx;
            Invoke("ColliderOnce", 2.0f);
            GetComponent<BoxCollider>().enabled = false;

        }
    }

    void ColliderOnce()
    {
        GetComponent<BoxCollider>().enabled = true;
    } 
}
